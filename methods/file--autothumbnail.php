<?php

return function($file, $width = null, $height = null) {

  $width = $width ? $width : c::get('kirbytext.image.width',null);
  $height = $height ? $height : c::get('kirbytext.image.height',null);

  $text = '(image: '. $file->filename() .' width: '. $width .' height:' . $height .')';

  return kirbytext( $text );

};
