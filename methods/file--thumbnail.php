<?php

return function($file, $options = array()) {

  /* Use this option if you want to override the default
   * configuration for the method. For example, you can
   * define new configuration variables such as:
   */
  $_options = a::update([
    // 'width'  => c::get('auto-thumbnail.image.width',null),
    // 'height' => c::get('auto-thumbnail.image.height',null),
    // 'ratio'  => c::get('auto-thumbnail.image.ratio',null),
    // 'usephotoswipe' => c::get('auto-thumbnail.photoswipe',null)
  ], $options);

  $text = '(image: '. $file->uri();
  foreach ($_options as $key => $value) {
    $text .= ' ' . $key .':'. $value;
  }
  $text .= ')';

  return kirbytext( $text );

};
