<?php

// image tag
return array(
  'attr' => array(
    'width',
    'height',
    'ratio',
    'alt',
    'text',
    'title',
    'class',
    'imgclass',
    'linkclass',
    'caption',
    'link',
    'target',
    'popup',
    'rel',
    'usephotoswipe',
    'usefigure',
    'uselazyload'
  ),
  'html' => function($tag) {

    $url     = $tag->attr('image');
    $alt     = $tag->attr('alt');
    $title   = $tag->attr('title');
    $link    = $tag->attr('link');
    $file    = $tag->file($url);
    $caption = $tag->attr('caption') === 'false' ? NULL : ( $tag->attr('caption') ?: ( $file->caption()->isNotEmpty() ? $file->caption() : NULL ) );
    $usefigure = $tag->attr('usefigure') ? ($tag->attr('usefigure') === 'false' ? false : true) : kirby()->option('kirbytext.image.figure');

    // use the file url if available and otherwise the given url
    $url = $file ? $file->url() : url($url);

    // alt is just an alternative for text
    if($text = $tag->attr('text')) $alt = $text;

    // try to get the title from the image object and use it as alt text
    if($file) {

      if(empty($alt) and $file->alt() != '') {
        $alt = $file->alt();
      }

      if(empty($title) and $file->title() != '') {
        $title = $file->title();
      }

    }

    // at least some accessibility for the image
    if(empty($alt)) $alt = ' ';

    // link builder
    $_link = function($image) use($tag, $url, $link, $file) {

      $isPhotoSwipeActive = $tag->attr('usephotoswipe') ? ($tag->attr('usephotoswipe') === 'false' ? false : true): c::get('kirbytext.photoswipe', null);

      if(empty($link) && $isPhotoSwipeActive !== true) return $image;

      // build the href for the link
      if($link == 'self' || $isPhotoSwipeActive === true) {
        $href = $url;
      } else if($file and $link == $file->filename()) {
        $href = $file->url();
      } else if($tag->file($link)) {
        $href = $tag->file($link)->url();
      } else {
        $href = $link;
      }

      $options = array(
        'rel'    => $tag->attr('rel'),
        'class'  => $tag->attr('linkclass'),
        'title'  => $tag->attr('title'),
        'target' => $tag->target()
      );

      if ( $isPhotoSwipeActive === true ) {

        $options['data-size'] = $file->width() .'x'. $file->height();

      }

      return html::a(url($href), $image, $options);

    };

    // image+thumb builder
    $_image = function($class) use($tag, $url, $file, $alt, $title) {

      $uselazyload = $tag->attr('uselazyload') ? ($tag->attr('uselazyload') === 'false' ? false : true) : kirby()->option('kirbytext.lazyload');
      $width       = $tag->attr('width')  ?: c::get('kirbytext.image.width',null);
      $height      = $tag->attr('height') ?: c::get('kirbytext.image.height',null);
      $ratio       = $tag->attr('ratio')  ?: c::get('kirbytext.image.ratio',null);
      $lzMaxSize   = 50;
      $lzwidth     = $lzMaxSize;
      $lzheight    = $lzMaxSize;

      if( $ratio > 0 ) {

        if ($file->ratio() < 1) {
          $width  = $height / $ratio; // vertical
        } else {
          $height = $width / $ratio; // horizontal
        }

        $lzdimensions = new Dimensions($width, $height);
        $lzdimensions->fit($lzMaxSize, true);
        $lzwidth  = $lzdimensions->width();
        $lzheight = $lzdimensions->height();

      }

      if($file && ($width || $height)) {

        $_attributs = array(
          'class' => $class,
          'title' => $title,
          'alt'   => $alt
        );

        $_thumb = thumb($file, array(
          'driver' => c::get('thumbs.driver', 'gd'),
          'root'   => kirby()->roots()->thumbs(),
          'width'  => $width,
          'height' => $height,
          'crop'   => $ratio > 0 ? true : false
        ));

        if ( $uselazyload === true ) {

          $_lazyThumb = thumb($file, array(
            'driver' => c::get('thumbs.driver', 'gd'),
            'root'   => kirby()->roots()->thumbs(),
            'width'  => $lzwidth,
            'height' => $lzheight,
            'blur'   => true,
            'blurpx' => 50,
            'crop'   => $ratio > 0 ? true : false
          ));

          $_attributs['data-src'] = $_thumb->url();
          $_attributs['onload'] = "lzld(this)";

          return html::img($_lazyThumb->url(), $_attributs);

        }

        else {

          return html::img($_thumb->url(), $_attributs);

        }


      }

      else {

        return html::img($url, array(
          'width'  => $width,
          'height' => $height,
          'class'  => $class,
          'title'  => $title,
          'alt'    => $alt
        ));

      }

    };

    // Add a class about image format.
    $_formatclass = function() use($tag, $url, $file) {
      if($file) {
        return $file->orientation();
      }
    };

    if($usefigure === true) {
      $image  = $_link($_image($tag->attr('imgclass')));
      $figure = new Brick('figure');
      $figure->addClass(trim($tag->attr('class') . ' ' . $_formatclass() ));
      $figure->append($image);
      if(!empty($caption)) {
        $figure->append('<figcaption>' . kirbytext($caption) . '</figcaption>');
      }
      return $figure;
    } else {
      $class = trim($tag->attr('class') . ' ' . $tag->attr('imgclass'). ' ' . $_formatclass() );
      return $_link($_image($class));
    }

  }
);
