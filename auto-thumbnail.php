<?php
/**
 * Auto-thumbnail Plugin
 *
 * Simply create a thumbnail of the image if a width (or height) param is set.
 * You can also use a default size with kirby options.
 * c::set('kirbytext.image.width')
 * c::set('kirbytext.image.height')
 * c::set('kirbytext.image.ratio')
 * c::set('kirbytext.lazyload')
 * c::set('kirbytext.photoswipe')
 *
 * @author Julien Gargot <julien@g-u-i.me>
 * @version 1.2.1
 */

$kirby->set('tag', 'image', include( __DIR__ . DS . 'tags' . DS . 'image.php' ));
$kirby->set('file::method', 'autothumbnail', include( __DIR__ . DS . 'methods' . DS . 'file--autothumbnail.php' )); // keep compatibility
$kirby->set('file::method', 'thumbnail',     include( __DIR__ . DS . 'methods' . DS . 'file--thumbnail.php' ));
$kirby->set('file::method', 'changeratio',   include( __DIR__ . DS . 'methods' . DS . 'file--changeratio.php' ));
$kirby->set('snippet', 'photoswipe', __DIR__ . '/snippets/photoSwipe.php');
