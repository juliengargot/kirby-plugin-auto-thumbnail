# Kirby plugins

## Auto-thumbnail Plugin

In the context of Kirbytext, the plugin simply creates a thumbnail of the image if a width or height parameter is set.

You can set default sizes, in the Kirby `config`, that will be used if no parameter is used in the Kirbytag:  
`c::set('kirbytext.image.width')`
`c::set('kirbytext.image.height')`
`c::set('kirbytext.image.ratio')`
`c::set('kirbytext.lazyload')`*
`c::set('kirbytext.photoswipe')`**

*\*Require the Lazyload JS library.*  
*\*\*Usefull if you use the PhotoSwipe JS library.*
